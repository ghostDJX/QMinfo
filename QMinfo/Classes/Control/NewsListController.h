//
//  NewsListController.h
//  QMinfo
//
//  Created by dongjun on 16/12/28.
//  Copyright © 2016年 ghost. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsListViewCell.h"
#import "LiuXSegmentView.h"
#import "NewsDetailController.h"

@interface NewsListController : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    NSMutableArray *listArray;
    NSArray *searchArray;
}
@property (strong, nonatomic) IBOutlet UITableView *table;

@end
