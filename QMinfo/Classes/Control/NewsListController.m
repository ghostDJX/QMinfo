//
//  NewsListController.m
//  QMinfo
//
//  Created by dongjun on 16/12/28.
//  Copyright © 2016年 ghost. All rights reserved.
//

#import "NewsListController.h"

@interface NewsListController ()

@end

@implementation NewsListController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_table registerNib:[UINib nibWithNibName:@"NewsListViewCell" bundle:nil] forCellReuseIdentifier:@"NewsListViewCell"];
    
    _table.delegate = self;
    
    _table.dataSource = self;
    
    _table.tableFooterView = [UIView new];
    
    //类型,,top(头条，默认),shehui(社会),guonei(国内),guoji(国际),yule(娱乐),tiyu(体育)junshi(军事),keji(科技),caijing(财经),shishang(时尚)
    searchArray = @[@"top",@"shehui",@"guonei",@"guoji",@"yule",@"tiyu",@"junshi",@"keji",@"caijing",@"shishang"];
    NSArray * titleArr = @[@"头条",@"社会",@"国内",@"国际",@"娱乐",@"体育",@"军事",@"科技",@"财经",@"时尚"];
    
    LiuXSegmentView *view=[[LiuXSegmentView alloc] initWithFrame:CGRectMake(0, 20, self.view.frame.size.width,40) titles:titleArr clickBlick:^void(NSInteger index) {
        NSLog(@"start-----%ld",(long)index);
        [self initData:searchArray[index-1]];
        NSLog(@"end-----%ld",(long)index);
    }];
    //以下属性可以根据需求修改
    view.titleFont=[UIFont fontWithName:@"Menlo-BoldItalic" size:18];
    //    view.defaultIndex=2;
    //    view.titleNomalColor=[UIColor blueColor];
    //    view.titleSelectColor=[UIColor orangeColor];
    [self.view addSubview:view];
    
    [self initData:@"top"];
}

//加载数据
-(void)initData:(NSString *)type{
    listArray = [[NSMutableArray alloc] init];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    
    NSMutableDictionary *data = [NSMutableDictionary new];
    [data setObject:type forKey:@"type"];
    [data setObject:@"fa7f3e9309c01a3ac7e731fd968a8096" forKey:@"key"];
    
    [manager GET:@"http://v.juhe.cn/toutiao/index" parameters:data success:^(AFHTTPRequestOperation *operation,id responseObject)
     {
         NSLog(@"信息:%@",responseObject);
         listArray = responseObject[@"result"][@"data"];
         if (listArray.count>0) {
             [_table reloadData];
         }
     }failure:^(AFHTTPRequestOperation *operation,NSError *error) {
         NSLog(@"Error: %@", error);
     }];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NewsListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NewsListViewCell"];
    if (!cell) {
        cell = [[NewsListViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"NewsListViewCell"];
    }
    if (indexPath.row<=listArray.count) {
        [cell initData:listArray[indexPath.row]];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 300;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  listArray.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *data = listArray[indexPath.row];
    NewsDetailController *detail = [[NewsDetailController alloc] initWithNibName:@"NewsDetailController" bundle:nil];
    detail.urlString = data[@"url"];
    DYBaseNC *nav = [[DYBaseNC  alloc]initWithRootViewController:detail];
    [self presentViewController:nav animated:YES completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
