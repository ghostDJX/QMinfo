//
//  NewsDetailController.h
//  QMinfo
//
//  Created by dongjun on 16/12/28.
//  Copyright © 2016年 ghost. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsDetailController : DYBaseVC<UIWebViewDelegate>
@property (strong, nonatomic) IBOutlet UIWebView *web;
@property (nonatomic,strong) NSString *urlString;
@end
