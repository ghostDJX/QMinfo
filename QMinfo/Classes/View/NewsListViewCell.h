//
//  NewsListViewCell.h
//  QMinfo
//
//  Created by dongjun on 16/12/28.
//  Copyright © 2016年 ghost. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsListViewCell : UITableViewCell{
    
    IBOutlet UIImageView *imageV;
    IBOutlet UILabel *titleV;
}

-(void)initData:(NSDictionary *)data;

@end
