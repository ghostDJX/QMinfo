//
//  NewsListViewCell.m
//  QMinfo
//
//  Created by dongjun on 16/12/28.
//  Copyright © 2016年 ghost. All rights reserved.
//

#import "NewsListViewCell.h"

@implementation NewsListViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    titleV.font = [UIFont fontWithName:@"Menlo-BoldItalic" size:15];
}

-(void)initData:(NSDictionary *)data{
    [imageV sd_setImageWithURL:[NSURL URLWithString:data[@"thumbnail_pic_s"]] placeholderImage:[UIImage imageNamed:@"null"]];
    titleV.text = data[@"title"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
