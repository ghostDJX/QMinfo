//
//  AppDelegate.h
//  QMinfo
//
//  Created by dongjun on 16/12/28.
//  Copyright © 2016年 ghost. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsListController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

