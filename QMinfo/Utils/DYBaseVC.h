//
//  DYBaseVC.h
//  DYCaiLiWang
//
//  Created by diyou on 15/10/13.
//  Copyright (c) 2015年 linbo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DYBaseVC : UIViewController
-(void)showBackImageAction;
- (void)hideBackImageAction;
-(void)popToLoginVC;
@end
